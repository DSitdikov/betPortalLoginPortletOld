package softrpo.quickstartportlet.spring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PriceProcessorService {
    private RestTemplate restTemplate;

    @Value("${api-urls.price-processor}")
    private String priceProcessorUrl;

    @Autowired
    PriceProcessorService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getEvents() {
        return restTemplate.getForObject(priceProcessorUrl + "/gambler/leaguesEvents", String.class);
    }
}
