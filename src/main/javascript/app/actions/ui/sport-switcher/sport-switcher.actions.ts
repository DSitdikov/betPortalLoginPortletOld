import { ActionCreator } from 'redux';
import { UiActionType } from '../ui-action-type';
import CommonAction from '../../common/types/common-action';

export const toggleExpandStateOfSport: ActionCreator<CommonAction> = (id: number) => ({
    type: UiActionType.TOGGLE_EXPAND_STATE_OF_SPORT,
    payload: id
});