export const MarketType = {
    MONEY_LINE: 'ML',
    DOUBLE_CHANCE: 'DC',
    BOTH_TO_SCORE: 'BTS',
    HANDICAP: 'Handicap',
    TOTALS: 'Totals',
    UNKNOWN: 'Unknown'
};