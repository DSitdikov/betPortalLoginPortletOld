import Price from '../../price/price';
import Market from '../common/market';
import { MarketType } from '../common/market-type';
import { MarketConstants } from '../../../../constants/market.constants';
import { OutcomeType } from '../../outcome/outcome-type';
import LeagueEvent from '../../league-event/league-event';

export default class HandicapMarket extends Market {
    protected outcomeTypes: string[] = [OutcomeType.HOME, OutcomeType.AWAY];
    hdp: number;

    constructor(data: any = {}) {
        super({
            ...data,
            type: MarketType.HANDICAP
        });

        this.hdp = Math.abs(data.hdp) || 0;
        const isHdpGreaterThanZero = 0 < this.hdp;
        const pointsFormatted = this.hdp.toFixed(MarketConstants.SIZE_OF_DECIMAL_PART);

        this.outcomeA = new Price({
            tip: `${isHdpGreaterThanZero ? '+' : '-'}${pointsFormatted}`,
            value: typeof data.outcomeA === 'object' ? data.outcomeA.value : data.home
        });
        this.outcomeB = new Price({
            tip: `${isHdpGreaterThanZero ? '-' : '+'}${pointsFormatted}`,
            value: typeof data.outcomeB === 'object' ? data.outcomeB.value : data.away
        });
    }

    protected selectOutcomeName(outcomeType: string, event: LeagueEvent): string {
        const isHome = outcomeType === OutcomeType.HOME;
        const team = isHome ? event.home : event.away;
        return `${team} ${isHome ? this.outcomeA.tip : this.outcomeB.tip}`;
    }
}
