import AppNotification from '../ui/app-notification/app-notification';
import AppError from '../../exceptions/app-error';
import EventUiState from '../ui/event-ui-state/event-ui-state';
import PeriodUiState from '../ui/period-ui-state/period-ui-state';
import LeagueUiState from '../ui/league-ui-state/league-ui-state';
import SportUiState from '../ui/sport-ui-state/sport-ui-state';
import MarketUiState from '../ui/market-ui-state/market-ui-state';

export default interface UiState {
    common: {
        busy: boolean,
        error?: AppError,
        notification?: AppNotification
    };
    sport: SportUiState;
    leagues: Map<number, LeagueUiState>;
    events: Map<number, EventUiState>;
    periods: Map<number, PeriodUiState>;
    markets: Map<number, MarketUiState>;
}