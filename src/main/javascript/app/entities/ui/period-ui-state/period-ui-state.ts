import { TotalType } from '../../domain-model/markets/total-market/total-type';

export default class PeriodUiState {
    selectedTotalsType: number;

    constructor(data: any = {}) {
        this.selectedTotalsType = data.selectedTotalsType || TotalType.OVERALL;
    }
}