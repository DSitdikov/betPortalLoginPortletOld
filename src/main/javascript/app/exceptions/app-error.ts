export default class AppError extends Error {
    code: string;
    message: string;

    constructor(data?: any) {
        super();

        data = data || {};

        if (typeof data === 'string') {
            data = { message: data };
        }

        this.code = data.code || 0;
        this.message = data.message || '';
    }
}