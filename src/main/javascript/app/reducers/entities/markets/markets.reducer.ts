import CommonAction from '../../../actions/common/types/common-action';
import Market from '../../../entities/domain-model/markets/common/market';
import LeagueEntities from '../../../services/data-loaders/league-data-loader/league-entities';
import { DomainModelActionType } from '../../../actions/domain-model/domain-model-action-type';

const initialState = new Map<number, Market>();

export default function marketsReducer(state: Map<number, Market> = initialState, action: CommonAction) {
    if (action.type === DomainModelActionType.FETCH_LEAGUES_SUCCESS) {
        const leagueEntities = action.payload as LeagueEntities;
        return leagueEntities.markets;
    }
    return state;
}