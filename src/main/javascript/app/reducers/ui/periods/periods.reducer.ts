import CommonAction from '../../../actions/common/types/common-action';
import PeriodUiState from '../../../entities/ui/period-ui-state/period-ui-state';
import { DomainModelActionType } from '../../../actions/domain-model/domain-model-action-type';
import LeagueEntities from '../../../services/data-loaders/league-data-loader/league-entities';
import { UiActionType } from '../../../actions/ui/ui-action-type';
import { TotalType } from '../../../entities/domain-model/markets/total-market/total-type';

const initialState = new Map<number, PeriodUiState>();

export default function periodsReducer(state: Map<number, PeriodUiState> = initialState, action: CommonAction) {
    switch (action.type) {
        case DomainModelActionType.FETCH_LEAGUES_SUCCESS: {
            const leagueEntities = action.payload as LeagueEntities;
            const periods = leagueEntities.periods;
            const resultState = new Map<number, PeriodUiState>(state);

            for (const periodId of Array.from(periods.keys())) {
                if (!resultState.has(periodId)) {
                    resultState.set(periodId, new PeriodUiState());
                }
            }

            return resultState;
        }
        case UiActionType.TOGGLE_TOTAL_TYPE: {
            const periodId = action.payload.periodId as number;
            const resultState = new Map<number, PeriodUiState>(state);

            const periodUiState = new PeriodUiState(resultState.get(periodId));

            // If selected the same total type, reset to Overall Totals
            const isSelectedTheSame = periodUiState.selectedTotalsType === action.payload.totalType;
            periodUiState.selectedTotalsType = isSelectedTheSame ? TotalType.OVERALL : action.payload.totalType;

            resultState.set(periodId, periodUiState);

            return resultState;
        }
        default:
            return state;
    }
}