import AppError from '../../../exceptions/app-error';
import env from '../../../env/env';
import DataLoader from '../common/data-loader';

export default class BasicDataLoader implements DataLoader {
    baseApiUrl: string;
    baseApiUrlSuffix: string;

    constructor(data?: any) {
        data = data || {};

        this.baseApiUrl = env.apiBaseUrl;
        this.baseApiUrlSuffix = data.baseApiUrlSuffix || '';
    }

    get(url: string): Promise<any> {
        return this.invokeRequest(url);
    }

    post(url: string, payload?: any): Promise<any> {
        const body = payload ? JSON.stringify(payload) : undefined;
        const additionalHeaders = env.mockRequestMethod ? { 'X-Dev-Body': body } : {};

        return this.invokeRequest(url, {
            method: !env.mockRequestMethod ? 'POST' : 'GET',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                ...additionalHeaders
            },
            body: !env.mockRequestMethod ? body : undefined
        });
    }

    private delay(time: number) {
        return result => new Promise(resolve => setTimeout(() => resolve(result), time));
    }

    private invokeRequest(url: string, params?: any): Promise<any> {
        const resultUrl = `${this.baseApiUrl}${this.baseApiUrlSuffix}${url}`;
        params = {
            ...params,
            credentials: 'same-origin'
        };

        let isOkResponse = false;
        return fetch(resultUrl, params)
            .then(this.delay(env.requestDelay))
            .then((response: any) => {
                const contentType = response.headers.get('Content-Type');
                const isCorrectContentType = contentType && contentType.includes('json');

                if (!isCorrectContentType) {
                    throw new AppError('Response has incorrect content type');
                }

                isOkResponse = response.ok;
                return response.json();
            })
            .then(rawData => {
                if (!isOkResponse || env.mockHttpStatusHandler && rawData.httpStatus && rawData.httpStatus !== 200) {
                    throw new AppError(rawData);
                }
                return rawData;
            })
            .catch(error => {
                console.error(error);
                throw new AppError(error);
            });
    }
}