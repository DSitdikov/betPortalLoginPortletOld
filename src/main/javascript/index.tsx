import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './register-service-worker';
import thunk from 'redux-thunk';
import { applyMiddleware, createStore } from 'redux';
import rootReducer from './app/reducers/index';
import { Provider } from 'react-redux';
import StoreState from './app/entities/store-state';
import { composeWithDevTools } from 'redux-devtools-extension';
import env from './app/env/env';
import { EnvType } from './app/enums/env-type';

const middleware = [
    thunk
];

const store = createStore<StoreState>(
    rootReducer,
    composeWithDevTools(applyMiddleware(...middleware))
);

const root = document.getElementById('putYourRootElementHere') as HTMLElement;

// Set basic constants
env.apiBaseUrl = root.getAttribute('data-base-resource-url') || env.apiBaseUrl;

ReactDOM.render(
    <Provider store={store}>
        <h1>Put your component here</h1>
    </Provider>,
    root
);

if (process.env.NODE_ENV !== EnvType.PROD) {
    registerServiceWorker();
}
